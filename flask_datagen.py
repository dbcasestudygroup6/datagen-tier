from flask import Flask, Response
from RandomDealData import *
from config import *

app = Flask(__name__)

def run_deal():
    data = RandomDealData()
    list = data.createInstrumentList()
    deal = data.createRandomData(list)
    return deal

@app.route('/streamTest')
def stream_send():
    def eventStream():
        while True:
            yield run_deal()+"\n"

    return Response(eventStream(), mimetype="text/event-stream")


def bootapp():
    app.run(port=8090, threaded=True, host=(DOCKER_HOST))


if __name__ == "__main__":
    bootapp()